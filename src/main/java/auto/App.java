package auto;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import helpers.Logger;
import tests.UserProfileFillTest;

public class App {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Mihaif92\\Desktop\\Automation Files\\chromedriver_win32\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.navigate().to("https://www.fxp.co.il/");
		
//		SigninAndSignoutTest signinAndSignoutTest = new SigninAndSignoutTest();
//		signinAndSignoutTest.run(driver);
		
		UserProfileFillTest userProfileFillTest = new UserProfileFillTest();
		userProfileFillTest.run(driver);
		
		Logger.log("\nProgram finished SUCCESSFULY");
	}

}
