package helpers;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Minion {

	public static void clickOnId(WebDriver driver, String id) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
		
		WebElement popUpCancel = wait
				.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
		
		popUpCancel = wait
				.until(ExpectedConditions.elementToBeClickable(By.id(id)));
		popUpCancel.click();

	}
}
