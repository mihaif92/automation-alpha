package helpers;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Connectivity {

	public static void signIn(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));

		WebElement userName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("navbar_username")));
		userName.click();
		userName.sendKeys("Boti");

		WebElement passwordFild = driver.findElement(By.id("navbar_password_hint"));
		passwordFild.click();

		WebElement password = driver.findElement(By.id("navbar_password"));
		password.click();
		password.sendKeys("123456");

		WebElement loginBtn = driver.findElement(By.cssSelector(
				"div.fixedlogin:nth-child(11) div.toplogin.graygr.dologin:nth-child(2) form:nth-child(1) span.log_in2:nth-child(2) > button.bluelogin"));
		loginBtn.click();

		Minion.clickOnId(driver, "onesignal-slidedown-cancel-button");

		WebElement userNameLogedin = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[contains(text(),'Boti')]")));

		if (userNameLogedin.isDisplayed()) {
			Logger.log("Loged-in Successfuly");
		} else {
			Logger.log("Login FAILED");
		}

	}

	public static void signOut(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

		WebElement settingsBtn = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@class='setopo initial loading']")));
		settingsBtn.click();

		WebElement logoutBtn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[23]//a[1]")));
		logoutBtn.click();

		WebElement homePageBtn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//body/div[@id='topmenunew']/div[@class='topbluew']/div[@class='first barssize bluegr']/ul[@id='top-menu']/li[1]/a[1]")));
		homePageBtn.click();

		WebElement userName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("navbar_username")));

		if (userName.isDisplayed()) {
			Logger.log("Loged-out Successfuly");
		} else {
			Logger.log("Logout FAILED");
		}

	}

}
