package tests;

import org.openqa.selenium.WebDriver;

import helpers.Connectivity;

public class SigninAndSignoutTest {

	public void run(WebDriver driver) {

		Connectivity.signIn(driver);
		
		Connectivity.signOut(driver);
	}
}
