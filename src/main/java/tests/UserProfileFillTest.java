package tests;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import helpers.Connectivity;
import helpers.Logger;

public class UserProfileFillTest {

	public void run(WebDriver driver) {

		Connectivity.signIn(driver);

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

		WebElement settingsBtn = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@class='setopo initial loading']")));
		settingsBtn.click();

		WebElement userProfileBtn = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[17]//a[1]")));
		userProfileBtn.click();

		userProfileClear(driver);
		userProfileCheck(driver, "emptyCheck");
		
		userProfileFill(driver);
		userProfileCheck(driver, "fillCheck");

		Connectivity.signOut(driver);

	}

	public void userProfileFill(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

		WebElement userName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cfield_2")));
		userName.sendKeys("Boti");

		WebElement FiledsOfIntrest = driver.findElement(By.id("cfield_3"));
		FiledsOfIntrest.sendKeys("Java and Automation");

		WebElement submitBtn = driver
				.findElement(By.xpath("//div[@class='blockfoot actionbuttons settings_form_border']//input[1]"));
		submitBtn.click();
		Logger.log("User Profile was updated");
	}

	public void userProfileClear(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

		WebElement userName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cfield_2")));
		userName.clear();

		WebElement FiledsOfIntrest = driver.findElement(By.id("cfield_3"));
		FiledsOfIntrest.clear();

		WebElement submitBtn = driver
				.findElement(By.xpath("//div[@class='blockfoot actionbuttons settings_form_border']//input[1]"));
		submitBtn.click();
		Logger.log("User Profile was cleared");

	}

	public void userProfileCheck(WebDriver driver, String status) {

		int counterTest = 0;
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

		WebElement userName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cfield_2")));
		String userNameInputBox = userName.getAttribute("value");

		WebElement FiledsOfIntrest = driver.findElement(By.id("cfield_3"));
		String FiledsOfIntrestInputBox = FiledsOfIntrest.getAttribute("value");

		if (status.equals("emptyCheck")) {

			if (userNameInputBox.isEmpty()) {
				counterTest++;
			}

			if (FiledsOfIntrestInputBox.isEmpty()) {
				counterTest++;
			}
		} else if (status.equals("fillCheck")) {
			if (userNameInputBox.equals("Boti")) {
				counterTest++;
			}

			if (FiledsOfIntrestInputBox.equals("Java and Automation")) {
				counterTest++;
			}
		}

		if (counterTest == 2) {
			Logger.log("Ueser profile was updated SUCCESSFULY");
		} else {
			Logger.log("Ueser profile FAILED to update");
		}
	}

}
